--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.3 (Debian 11.3-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: projek; Type: SCHEMA; Schema: -; Owner: projek
--

CREATE SCHEMA projek;

ALTER SCHEMA projek OWNER TO projek;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Produk; Type: TABLE; Schema: projek; Owner: projek
--

CREATE TABLE projek."Produk" (
    id integer NOT NULL,
    nama character varying(30) NOT NULL,
    harga double precision DEFAULT '0'::double precision NOT NULL
);


ALTER TABLE projek."Produk" OWNER TO projek;

--
-- Name: TABLE "Produk"; Type: COMMENT; Schema: projek; Owner: projek
--

COMMENT ON TABLE projek."Produk" IS 'Produk stores all the information required for login';


--
-- Name: Produk_id_seq; Type: SEQUENCE; Schema: projek; Owner: projek
--

CREATE SEQUENCE projek."Produk_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE projek."Produk_id_seq" OWNER TO projek;

--
-- Name: Produk_id_seq; Type: SEQUENCE OWNED BY; Schema: projek; Owner: projek
--

ALTER SEQUENCE projek."Produk_id_seq" OWNED BY projek."Produk".id;


--
-- Name: Produk id; Type: DEFAULT; Schema: projek; Owner: projek
--

ALTER TABLE ONLY projek."Produk" ALTER COLUMN id SET DEFAULT nextval('projek."Produk_id_seq"'::regclass);


--
-- Name: Produk Produk_pkey; Type: CONSTRAINT; Schema: projek; Owner: projek
--

ALTER TABLE ONLY projek."Produk"
    ADD CONSTRAINT "Produk_pkey" PRIMARY KEY (id);
	
--
-- Data for Name: Produk; Type: TABLE DATA; Schema: projek; Owner: projek
--

INSERT INTO projek."Produk" (id, nama, harga) VALUES (1, 'HP Samsung', 10000000);

--
-- PostgreSQL database dump complete
--