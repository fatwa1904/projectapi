const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const db = require('./conn')
const port = 3000


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.get('/', (request, response) => {
    response.json({ info: 'Berhasil Tampil ' })
  });


app.get('/users', db.getUsers)


app.listen(port);
console.log('berhasil tampil ' + port);